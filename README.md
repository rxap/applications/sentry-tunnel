[![Image Size](https://img.shields.io/docker/image-size/rxap/sentry-tunnel)](https://hub.docker.com/r/rxap/sentry-tunnel)
[![Docker Pulls](https://img.shields.io/docker/pulls/rxap/sentry-tunnel)](https://hub.docker.com/r/rxap/sentry-tunnel)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

RxAP Sentry Tunnel
===

> A tunnel is an HTTP endpoint that acts as a proxy between Sentry and your application. Because you control this
> server, there is no risk of any requests sent to it being blocked. When the endpoint lives under the same origin (
> although it does not have to in order for the tunnel to work), the browser will not treat any requests to the endpoint
> as a third-party request. As a result, these requests will have different security measures applied which, by default,
> don't trigger ad-blockers. A quick summary of the flow can be found below.

![](https://docs.sentry.io/static/0abf35e833d5c96e4df6ade60361ad92/c1b63/tunnel.png)

[From the sentry documentation](https://docs.sentry.io/platforms/javascript/troubleshooting/#using-the-tunnel-option)

Please note that the **minimal supported Relay version is v21.6.0**. Older versions might work, but are not supported by
this project.
[Explanation here](https://develop.sentry.dev/sdk/envelopes/#authentication)

# Deployment

```yaml
version: "3.7"
services:
  sentry-tunnel:
    image: rxap/sentry-tunnel:${CHANNEL:-development}
    ports:
      - "3333:3333"
    environment:
      - SENTRY_HOST=your-sentry
      - SENTRY_PROJECT_LIST=45,3,5,1
    restart: unless-stopped
    networks:
      - default
      - traefik
```

## Traefik

```yaml
version: "3.7"
services:
  sentry-tunnel:
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.sentry-tunnel.entrypoints=https"
      - "traefik.http.routers.sentry-tunnel.rule=PathPrefix(`/tunnel`)"
      - "traefik.http.routers.sentry-tunnel.priority=9999"
      - "traefik.http.routers.sentry-tunnel.middlewares=sentry-tunnel"
      - "traefik.http.middlewares.sentry-tunnel.stripprefix.prefixes=/tunnel"
      - "traefik.http.services.sentry-tunnel.loadbalancer.server.port=3333"
    image: rxap/sentry-tunnel:${CHANNEL:-development}
    environment:
      - SENTRY_HOST=your-sentry
      - SENTRY_PROJECT_LIST=45,3,5,1
    restart: unless-stopped
    networks:
      - default
      - traefik
```

# Environment Variables

| Name                | Default       | Description |
|---------------------|---------------|-------------|
| PORT                | 3333          |             |
| GLOBAL_API_PREFIX   | -             |             |
| SENTRY_DSN          | -             |             |
| SENTRY_ENABLED      | false         |             |
| SENTRY_ENVIRONMENT  | (auto)        |             |
| SENTRY_RELEASE      | (auto)        |             |
| SENTRY_SERVER_NAME  | sentry-tunnel |             |
| SENTRY_DEBUG        | false         |             |
| SENTRY_HOST         | -             |             |
| SENTRY_PROJECT_LIST | -             |             |
