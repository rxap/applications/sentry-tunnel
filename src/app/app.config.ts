import { GenerateRandomString } from '@rxap/utilities';
import * as Joi from 'joi';
import { SchemaMap } from 'joi';

const validationSchema: SchemaMap = {};
validationSchema['COOKIE_SECRET'] = Joi.string().default(GenerateRandomString());
validationSchema['THROTTLER_LIMIT'] = Joi.string().default(10);
validationSchema['THROTTLER_TTL'] = Joi.string().default(1);
validationSchema['GLOBAL_API_PREFIX'] = Joi.string();
validationSchema['SENTRY_ENABLED'] = Joi.boolean().default(true);
validationSchema['SENTRY_HOST'] = Joi.string().required();
validationSchema['SENTRY_PROJECT_LIST'] = Joi.string().pattern(/^([0-9]+)?(,[0-9]+)*$/).default('');
validationSchema['PORT'] = Joi.number().default(3333);
export const VALIDATION_SCHEMA = Joi.object(validationSchema);
