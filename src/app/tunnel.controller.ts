import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  Logger,
  Post,
  Req,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AxiosError } from 'axios';
import { Request } from 'express';
import rawbody from 'raw-body';
import { firstValueFrom } from 'rxjs';
import { URL } from 'url';

@Controller('tunnel')
export class TunnelController {

  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
    private readonly logger: Logger,
  ) {
  }

  @Get()
  info() {
    return { host: this.config.get('SENTRY_HOST'), projectIdList: this.config.get('SENTRY_PROJECT_LIST').split(',') };
  }

  @Post()
  async tunnel(@Req() request: Request) {
    if (!this.config.get('SENTRY_ENABLED')) {
      return { message: 'Sentry tunnel is disabled' };
    }
    if (!request.readable) {
      throw new BadRequestException('Body has not the content type text/plain');
    }
    const envelope = (
      await rawbody(request)
    ).toString();
    this.logger.verbose(`Received envelope: >>>${ envelope }<<<`, 'TunnelController');
    const piece = envelope.trim().split('\n')[0];
    const header   = JSON.parse(piece);
    const dsn      = new URL(header.dsn);
    if (this.config.get('SENTRY_HOST') !== dsn.hostname) {
      this.logger.error(`Invalid sentry host: ${dsn.hostname}`, 'TunnelController');
      throw new BadRequestException('Invalid sentry host');
    }
    const projectId            = dsn.pathname.replace(/^\//, '');
    const allowedProjectIdList = this.config.getOrThrow('SENTRY_PROJECT_LIST').split(',');
    if (!allowedProjectIdList.includes(projectId)) {
      this.logger.error(`Invalid sentry project id: ${projectId}`, 'TunnelController');
      throw new BadRequestException('Invalid sentry project id');
    }
    const url = `https://${dsn.hostname}/api/${projectId}/envelope/`;
    this.logger.log(`Send envelope to ${dsn.hostname} for project ${projectId}`, 'TunnelController');
    try {
      const response = await firstValueFrom(this.http.post(url, envelope, {
        headers: {
          'Content-Type': 'text/plain;charset=UTF-8',
        },
      }));
      this.logger.verbose(`Sentry response: ${ JSON.stringify(response.data) }`, 'TunnelController');
      return response.data;
    } catch (e: any) {
      if (e instanceof AxiosError) {
        if (e.response) {
          this.logger.error(
            `Sentry request failed with network error and data: ${ JSON.stringify(e.response.data) }`,
            'TunnelController',
          );
        } else {
          this.logger.error(
            `Sentry request failed with network error: ${e.message}`,
            'TunnelController',
          );
        }
        throw new InternalServerErrorException('Upstream API request failed');
      }
      this.logger.error(
        `Sentry request failed: ${e.message}`,
        'TunnelController',
      );
      throw new InternalServerErrorException(e);
    }
  }

}
