import { HttpModule } from '@nestjs/axios';
import { CacheModule } from '@nestjs/cache-manager';
import {
  Logger,
  Module,
} from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import {
  ThrottlerGuard,
  ThrottlerModule,
} from '@nestjs/throttler';
import {
  CacheModuleOptionsLoader,
  ENVIRONMENT,
  ThrottlerModuleOptionsLoader,
} from '@rxap/nest-utilities';
import { environment } from '../environments/environment';
import { VALIDATION_SCHEMA } from './app.config';
import { AppController } from './app.controller';
import { HealthModule } from './health/health.module';
import { TunnelController } from './tunnel.controller';

@Module({
  imports: [
    ThrottlerModule.forRootAsync(
      {
        useClass: ThrottlerModuleOptionsLoader,
      }),
    ConfigModule.forRoot(
      {
        isGlobal: true,
        validationSchema: VALIDATION_SCHEMA,
      }),
    HttpModule,
    HealthModule,
    CacheModule.registerAsync(
      {
        isGlobal: true,
        useClass: CacheModuleOptionsLoader,
      }),
  ],
  controllers: [
    AppController,
    TunnelController,
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: ThrottlerGuard,
    },
    {
      provide: ENVIRONMENT,
      useValue: environment,
    },
    Logger,
  ],
})
export class AppModule {}
