import {
  Controller,
  Get,
  Inject,
} from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import {
  HealthCheck,
  HealthCheckResult,
  HealthCheckService,
} from '@nestjs/terminus';
import { Public } from '@rxap/nest-utilities';
import { SentryHealthIndicator } from './sentry.health-indicator';

@Controller('health')
@Public()
@ApiExcludeController()
export class HealthController {
  @Inject(SentryHealthIndicator)
  private readonly sentryHealthIndicator: SentryHealthIndicator;

  @Inject(HealthCheckService)
  private readonly health: HealthCheckService;

  @Get('sentry')
  @HealthCheck()
  public sentry(): Promise<HealthCheckResult> {
    return this.health.check([
      async () => this.sentryHealthIndicator.isHealthy(),
    ]);
  }

  @Get()
  @HealthCheck()
  public healthCheck(): Promise<HealthCheckResult> {
    return this.health.check([
      async () => this.sentryHealthIndicator.isHealthy(),
    ]);
  }

}
