import { Injectable } from '@nestjs/common';
import {
  HealthIndicator,
  HealthIndicatorResult,
} from '@nestjs/terminus';

@Injectable()
export class SentryHealthIndicator extends HealthIndicator {
  constructor() {
    super();
  }

  public async isHealthy(): Promise<HealthIndicatorResult> {
    return this.getStatus('sentry', true);
  }

}
