import {
  Logger,
  Module,
} from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthController } from './health.controller';
import { SentryHealthIndicator } from './sentry.health-indicator';

@Module({
  imports: [TerminusModule],
  providers: [Logger, SentryHealthIndicator],
  controllers: [HealthController],
})
export class HealthModule {
}
