import { NestApplicationOptions } from '@nestjs/common';
import { NestExpressApplication } from '@nestjs/platform-express';
import { RxapLogger } from '@rxap/nest-logger';
import {
  Monolithic,
  MonolithicBootstrapOptions,
  SetupCookieParser,
  SetupCors,
  SetupHelmet,
  ValidationPipeSetup,
} from '@rxap/nest-server';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

const options: NestApplicationOptions = {};
const bootstrapOptions: Partial<MonolithicBootstrapOptions> = {};
const server = new Monolithic<NestApplicationOptions, NestExpressApplication>(
  AppModule,
  environment,
  options,
  bootstrapOptions,
);
server.after(SetupHelmet());
server.after(SetupCookieParser());
server.after(SetupCors());
server.after((app) => app.useLogger(new RxapLogger()));
server.after(ValidationPipeSetup());
server
  .bootstrap()
  .catch((e) => console.error('Server bootstrap failed: ' + e.message));
